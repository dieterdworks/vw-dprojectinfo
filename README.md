VW DProjectInfo
========================================================================================================================

Project info administration with automatic fills.

## Initial Setup #######################################################################################################

- Make sure you add the latest version of `dlibrary` to the project libraries.
- Make sure you add the latest version of `dlibrary` to the Python Script paths in Vectorworks.

## Branches ############################################################################################################

We'll use the same methodology as `dlibrary`. So we'll end up with three kind of branches:

- The MASTER branch. (Where all the main versions will be tagged.)
- The HISTORY branches. (`history/2015` Where older versions can be changed for hot-fixes.)
- The FEATURE branches. (`feature/feature-name` Where we work on new features, as they can span multiple versions.)

## Version numbers #####################################################################################################

We'll use semantic versioning, but we have to clarify if a version update is just because new dongles are added or not!
Therefore we'll use a version suffix like: `v2015.0.0-d4`, where the number after `d` is just a counter.
This dongle part will not be used anywere else! As the users don't have to know this, it's just for use to know.

## Release Checklist ###################################################################################################

- On regular release:
    - Update hard-coded version number on the plugin.
    - Commit updated version number.

- On new donlge release:
    - Update licensed dongle numbers on the security.
    - Commit new donlge numbers.

- Create a tag with the new version number.

- If regular release:
    - Create new version on Bitbucket, so issues can be logged with the correct version.

## Distribution Checklist ##############################################################################################

- As external .py files don't get encrypted well, we'll copy it's contents into the plugin file before releasing,
    replacing the import statement. This will ensure us that no one can get the source code.
- Encrypt the plugin through Vectorworks (Ctrl + Shift + Alt + Edit button).
- Create distribution zip with the following included:
    - DProjectInfo
        - DProjectInfo.vsm (encrypted)
        - DProjectInfoMainDialog.xml
        - DProjectInfoPrefs.xml
    - install.py (Copy from latest `dlibrary` and setup all install steps.)
- Upload zip file with correct name (DProjectInfo v2015.0.0) to www.dworks.be
- Revert all changes to be able to continue development.
