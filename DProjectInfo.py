import copy
import re
from _datetime import datetime

from dlibrary.criteria import Criteria
from dlibrary.dialog_custom import Dialog, AbstractActivePlugInDialogXmlFile
from dlibrary.dialog_predefined import AlertType, PlugInFileVsExceptionAlert, PlugInFileFileNotFoundErrorAlert, \
    PlugInFilePermissionErrorAlert, PlugInFileOsErrorAlert
from dlibrary.dialog_predefined import Alert
from dlibrary.document import Document, DesignLayer, SheetLayer, Units, RecordDefinitionResourceList, \
    AbstractResourceList, RecordDefinition
from dlibrary.object import AbstractObject, Record
from dlibrary.utility import VSException, SingletonMeta, XmlFileDefaults, XmlFileLists, AbstractViewModel, \
    LinkedObservableField, ObservableField, ObservableList, ObservableMethod, ViewModelList, ObservableCommand
from dlibrary.vectorworks import Security, AbstractActivePlugInPrefsXmlFile, ActivePlugInType, \
    AbstractActivePlugInDrawingXmlFile, ActivePlugInInfo, ActivePlugIn

__author__ = 'Dieter Geerts <dieter@dworks.be>'
__version__ = '2016.0.0'
__license__ = 'Private'

import pydevd
pydevd.settrace('localhost', port=8080, stdoutToServer=True, stderrToServer=True, suspend=False)


def __load_prefs_and_create_dialog() -> (dict, dict, Dialog):
    try:
        plugin_prefs = DProjectInfoPrefsXmlFile().load(True)
        drawing_prefs = DProjectInfoDrawingXmlFile().load(True)
        __fill_drawing_prefs_with_defaults_if_empty(drawing_prefs['prefs'], plugin_prefs['prefs'])
        dialog = Dialog(DProjectInfoMainDialogXmlFile(), DProjectInfoViewModel(plugin_prefs['prefs'],
                                                                               drawing_prefs['prefs']))
    except (VSException, FileNotFoundError, PermissionError, OSError):
        raise
    else:
        return plugin_prefs, drawing_prefs, dialog


def __fill_drawing_prefs_with_defaults_if_empty(drawing_prefs: dict, plugin_prefs: dict):
    if len(drawing_prefs['info']) == 0:
        drawing_prefs['info'] = copy.deepcopy(plugin_prefs['info'])
    if len(drawing_prefs['mapping']) == 0:
        drawing_prefs['mapping'] = copy.deepcopy(plugin_prefs['mapping'])


def __save_preferences(plugin_prefs: dict, drawing_prefs: dict):
    try:
        DProjectInfoPrefsXmlFile().save(plugin_prefs)
        DProjectInfoDrawingXmlFile().save(drawing_prefs)
    except (VSException, FileNotFoundError) as e:
        if isinstance(e, VSException):
            PlugInFileVsExceptionAlert(ActivePlugIn().name).show()
        elif isinstance(e, FileNotFoundError):
            PlugInFileFileNotFoundErrorAlert(e.filename).show()
        Alert(AlertType.CRITICAL, 'Due to the previous exception, we couldn\'t save your data').show()


def __update_project_fields(project_prefs: dict):
    DataProcessor().initialize({info['@name']: info['@value'] for info in project_prefs['info']})
    records = dict()  # Group by record to minimize object traversing.
    for mapping in project_prefs['mapping']:
        if mapping['@record'] not in records:
            records[mapping['@record']] = dict()
        records[mapping['@record']][mapping['@field']] = mapping['@value']
    for record, fields in records.items():
        for obj in Criteria().has_record(record).get():
            if isinstance(obj, AbstractObject):
                __update_record_fields(obj.records[record], fields, obj)


def __update_record_fields(record: Record, fields: dict, obj: AbstractObject):
    for field, value in fields.items():
        replacement = lambda match: DataProcessor().process(match.group(0)[2:-2], obj)
        record.fields[field].value = re.sub(r'{{.+?}}', replacement, value)


# - PLACEHOLDERS -------------------------------------------------------------------------------------------------------
# {{PLACEHOLDER_DATA_SOURCE|FILTER::FILTER_PARAM|FILTER::FILTER_PARAM::FILTER_PARAM}}
#
# PLACEHOLDER_DATA_SOURCES:
#   #NOW                    >> Returns the current date and time.
#   #FILENAME               >> Returns the file name of the current document, with extension.
#   #PAGEAREA               >> Returns the page area of the layer in document area units.
#   #LAYER_NAME             >> Returns the name/number of the design/sheet layer.
#   #LAYER_SCALE            >> Returns the scale of the layer, which is 1:1 for a sheet layer.
#   #LAYER_DESCRIPTION      >> Returns the description of the layer.
#   #VIEWPORT_TITLES        >> Returns a list of all viewport titles from the layer.
#   #VIEWPORT_TITLES_SCALES >> Returns a list of all viewport titles with there scales like Title '[1:100]'.
#   #VIEWPORT_SCALES        >> Returns a list of all distinct viewport scales from the layer.
#   #SHEET_TITLE            >> Returns the title of the sheet layer.
#   #NUM_SHEETS(x)          >> Returns the number of sheet layers which match the first x characters of their number.
#   INFO_NAME               >> Returns the info with this name, or the string itself if not found.
#
# FILTERS:
#   DATETIME::format                >> Format with given format string if data is datetime object.
#                                   >> The format can use Python placeholders, see
#                                      https://docs.python.org/2/library/datetime.html#strftime-and-strptime-behavior
#   NO_FILE_EXTENSION               >> Will remove the file extension, if present.
#   TO_UPPERCASE                    >> Transform to uppercase string.
#   TO_LOWERCASE                    >> Transform to lowercase string.
#   FIRST::amount                   >> Use only the first characters or first items in the list.
#   LAST::amount                    >> Use only the last characters or items in the list.
#   PREFIX::with                    >> Prefix with some characters for a string or each item in the list.
#   SUFFIX::with                    >> Suffix with some characters for a string or each item in the list.
#   IF_GREATER_THAN::x::replacement >> If the length of the string or list is greater thena mount, use replacement.
#   JOIN::by                        >> Join all items in the list into a sentence and put by between them.
# ----------------------------------------------------------------------------------------------------------------------


class DataProcessor(object, metaclass=SingletonMeta):
    def __init__(self):
        self.__infos = None

    def initialize(self, infos: dict):
        self.__infos = infos

    def process(self, data_sequence_string: str, obj: AbstractObject) -> str:
        data_sequence = data_sequence_string.split('|')
        return DataProcessor.__format_data(self.__get_source_data(data_sequence[0], obj), data_sequence[1:])

    def __get_source_data(self, data_source_string: str, obj: AbstractObject):
        if data_source_string == '#NOW':
            return datetime.today()
        elif data_source_string == '#FILENAME':
            return Document().filename
        elif data_source_string == '#PAGEAREA':
            return Units.to_area_string(obj.layer.drawing_area, True)
        elif data_source_string == '#LAYER_NAME':
            return obj.layer.name
        elif data_source_string == '#LAYER_SCALE':
            # noinspection PyUnresolvedReferences
            return self.__scale2string(obj.layer.scale if isinstance(obj.layer, DesignLayer) else 1)
        elif data_source_string == '#LAYER_DESCRIPTION':
            return obj.layer.description
        elif data_source_string == '#VIEWPORT_TITLES':
            return [viewport.title for viewport in Criteria().on_layer(obj.layer).is_viewport().get()]
        elif data_source_string == '#VIEWPORT_TITLES_SCALES':
            return ['%s [%s]' % (viewport.title, self.__scale2string(viewport.scale))
                    for viewport in Criteria().on_layer(obj.layer).is_viewport().get()]
        elif data_source_string == '#VIEWPORT_SCALES':
            return list({self.__scale2string(viewport.scale)
                         for viewport in Criteria().on_layer(obj.layer).is_viewport().get()})
        elif data_source_string == '#SHEET_TITLE':
            # noinspection PyUnresolvedReferences
            return obj.layer.title if isinstance(obj.layer, SheetLayer) else ''
        elif data_source_string[:-3] == '#NUM_SHEETS':
            prefix = obj.layer.name[:int(data_source_string[-2])]
            return len([sheet for sheet in Document().sheet_layers if sheet.name[:len(prefix)] == prefix])
        else:
            return self.__infos.get(data_source_string, data_source_string)

    @staticmethod
    def __scale2string(scale: float) -> str:
        return '1:%.0f' % scale if scale >= 1 else ('x%.0f' if 1/scale == int(1/scale) else 'x%.2f') % (1/scale)

    @staticmethod
    def __format_data(data, formatter_strings: list) -> str:
        for formatter_string in formatter_strings:
            formatter_sequence = formatter_string.split('::')
            data = DataProcessor.__get_formatter_function(formatter_sequence[0])(data, formatter_sequence[1:])
        return str(data)

    @staticmethod
    def __get_formatter_function(formatter_function_string: str) -> callable:
        return {
            'DATETIME':             DataProcessor.__format_datetime,
            'NO_FILE_EXTENSION':    DataProcessor.__format_no_file_extension,
            'TO_UPPERCASE':         DataProcessor.__format_to_uppercase,
            'TO_LOWERCASE':         DataProcessor.__format_to_lowercase,
            'FIRST':                DataProcessor.__format_first,
            'LAST':                 DataProcessor.__format_last,
            'PREFIX':               DataProcessor.__format_prefix,
            'SUFFIX':               DataProcessor.__format_suffix,
            'IF_GREATER_THAN':      DataProcessor.__format_if_greater_than,
            'JOIN':                 DataProcessor.__format_join
        }.get(formatter_function_string, DataProcessor.__format_nothing)

    @staticmethod
    def __format_nothing(data, _: list):
        return data

    @staticmethod
    def __format_datetime(data, params: list):
        return data.strftime(params[0]) if isinstance(data, datetime) and len(params) > 0 else data

    @staticmethod
    def __format_no_file_extension(data, _: list):
        return data[:-4] if isinstance(data, str) and data[-4:] == '.vwx' else data

    @staticmethod
    def __format_to_uppercase(data, _: list):
        return data.upper() if isinstance(data, str) else data

    @staticmethod
    def __format_to_lowercase(data, _: list):
        return data.lower() if isinstance(data, str) else data

    @staticmethod
    def __format_first(data, params: list):
        return data[:int(params[0])] if isinstance(data, str) or isinstance(data, list) else data

    @staticmethod
    def __format_last(data, params: list):
        return data[-int(params[0]):] if isinstance(data, str) or isinstance(data, list) else data

    @staticmethod
    def __format_prefix(data, params: list):
        if isinstance(data, str):
            return '%s%s' % (params[0], data)
        elif isinstance(data, list):
            return ['%s%s' % (params[0], item) for item in data]
        else:
            return data

    @staticmethod
    def __format_suffix(data, params: list):
        if isinstance(data, str):
            return '%s%s' % (data, params[0])
        elif isinstance(data, list):
            return ['%s%s' % (item, params[0]) for item in data]
        else:
            return data

    @staticmethod
    def __format_if_greater_than(data, params: list):
        return params[1] if (isinstance(data, str) or isinstance(data, list)) and len(data) > int(params[0]) else data

    @staticmethod
    def __format_join(data, params: list):
        return params[0].join(data) if isinstance(data, list) else data


class DProjectInfo(object, metaclass=SingletonMeta):
    def __init__(self):
        self.__record_definition_resource_list = None
        self.initialize()

    def initialize(self):
        self.__record_definition_resource_list = RecordDefinitionResourceList()

    @property
    def record_definition_resource_list(self) -> AbstractResourceList:
        return self.__record_definition_resource_list

    @staticmethod
    def get_default_prefs() -> dict:
        return {'prefs': {'info': [DProjectInfo.create_info()], 'mapping': [DProjectInfo.create_mapping()]}}

    @staticmethod
    def create_info() -> dict:
        return {'@name': '', '@value': ''}

    @staticmethod
    def info_repr(info: dict):
        return "Info(%s)" % info['@name']

    @staticmethod
    def create_mapping() -> dict:
        return {'@record': None, '@field': None, '@value': ''}

    @staticmethod
    def mapping_repr(mapping: dict):
        return "Mapping(%s, %s)" % (mapping['@record'], mapping['@field'])


@XmlFileDefaults(defaults=DProjectInfo.get_default_prefs())
@XmlFileLists(lists={'info', 'mapping'})
class DProjectInfoPrefsXmlFile(AbstractActivePlugInPrefsXmlFile):

    def __init__(self):
        super().__init__(ActivePlugInType.MENU)


@XmlFileDefaults(defaults=DProjectInfo.get_default_prefs())
@XmlFileLists(lists={'info', 'mapping'})
class DProjectInfoDrawingXmlFile(AbstractActivePlugInDrawingXmlFile):

    def __init__(self):
        super().__init__()


class DProjectInfoMainDialogXmlFile(AbstractActivePlugInDialogXmlFile):

    def __init__(self):
        super().__init__('Main', ActivePlugInType.MENU)


class InfoViewModel(AbstractViewModel):
    def __init__(self, info: dict):
        super().__init__(info)
        self.__name = LinkedObservableField(info, '@name')
        self.__value = LinkedObservableField(info, '@value')

    @property
    def name(self) -> ObservableField:
        return self.__name

    @property
    def value(self) -> ObservableField:
        return self.__value


class MappingViewModel(AbstractViewModel):
    def __init__(self, mapping: dict):
        super().__init__(mapping)
        self.__record = LinkedObservableField(mapping, '@record')
        self.__field = LinkedObservableField(mapping, '@field')
        self.__value = LinkedObservableField(mapping, '@value')
        self.__available_fields = ObservableList()
        self.__record.field_changed_event.subscribe(self.__on_record_changed)

    @property
    def record(self) -> ObservableField:
        return self.__record

    @property
    def field(self) -> ObservableField:
        return self.__field

    @property
    def value(self) -> ObservableField:
        return self.__value

    @property
    def available_fields(self) -> ObservableList:
        return self.__available_fields

    # noinspection PyUnusedLocal
    def __on_record_changed(self, old_value, new_value):
        self.__field.value = None
        self.__available_fields.suspend_events()
        self.__available_fields.clear()
        record_definition = DProjectInfo().record_definition_resource_list.get_resource(new_value)
        if record_definition is not None and isinstance(record_definition, RecordDefinition):
            self.__available_fields.extend(record_definition.fields)
        self.__available_fields.resume_events()


class DProjectInfoViewModel(object):
    def __init__(self, default_prefs: dict, project_prefs: dict):
        self.__default_prefs = default_prefs
        self.__project_prefs = project_prefs
        self.__can_never_change = ObservableMethod(lambda: True)
        self.__default_infos = ViewModelList(default_prefs['info'], InfoViewModel, DProjectInfo.create_info,
                                             self.__can_add_default_info, {'name'})
        self.__default_infos_field = ObservableField(self.__default_infos)
        self.__default_mappings = ViewModelList(default_prefs['mapping'], MappingViewModel, DProjectInfo.create_mapping,
                                                self.__can_add_default_mapping, {'record', 'field'})
        self.__default_mappings_field = ObservableField(self.__default_mappings)
        self.__project_infos = ViewModelList(project_prefs['info'], InfoViewModel, DProjectInfo.create_info,
                                             self.__can_add_project_info, {'name'})
        self.__project_infos_field = ObservableField(self.__project_infos)
        self.__project_mappings = ViewModelList(project_prefs['mapping'], MappingViewModel, DProjectInfo.create_mapping,
                                                self.__can_add_project_mapping, {'record', 'field'})
        self.__project_mappings_field = ObservableField(self.__project_mappings)
        self.__copy_infos_to_defaults_command = ObservableCommand(self.__copy_infos_to_defaults,
                                                                  self.__can_copy_infos_to_defaults,
                                                                  [self.__project_infos.selected_items])
        self.__copy_infos_to_project_command = ObservableCommand(self.__copy_infos_to_project,
                                                                 self.__can_copy_infos_to_project,
                                                                 [self.__default_infos.selected_items])
        self.__copy_mappings_to_defaults_command = ObservableCommand(self.__copy_mappings_to_defaults,
                                                                     self.__can_copy_mappings_to_defaults,
                                                                     [self.__project_mappings.selected_items])
        self.__copy_mappings_to_project_command = ObservableCommand(self.__copy_mappings_to_project,
                                                                    self.__can_copy_mappings_to_project,
                                                                    [self.__default_mappings.selected_items])

    @property
    def can_never_change(self) -> ObservableMethod:
        return self.__can_never_change

    @property
    def record_definition_resource_list(self) -> AbstractResourceList:
        return DProjectInfo().record_definition_resource_list

    @property
    def default_infos(self) -> ObservableField:
        return self.__default_infos_field

    @property
    def default_mappings(self) -> ObservableField:
        return self.__default_mappings_field

    @property
    def project_infos(self) -> ObservableField:
        return self.__project_infos_field

    @property
    def project_mappings(self) -> ObservableField:
        return self.__project_mappings_field

    @property
    def copy_infos_to_defaults(self) -> ObservableCommand:
        return self.__copy_infos_to_defaults_command

    @property
    def copy_infos_to_project(self) -> ObservableCommand:
        return self.__copy_infos_to_project_command

    @property
    def copy_mappings_to_defaults(self) -> ObservableCommand:
        return self.__copy_mappings_to_defaults_command

    @property
    def copy_mappings_to_project(self) -> ObservableCommand:
        return self.__copy_mappings_to_project_command

    def __can_add_default_info(self, info: dict):
        return self.__is_valid_info(info) and self.__is_name_unique(info, self.__default_prefs['info'])

    def __can_add_default_mapping(self, mapping: dict):
        return self.__is_valid_mapping(mapping) and self.__is_mapping_unique(mapping, self.__default_prefs['mapping'])

    def __can_add_project_info(self, info: dict):
        return self.__is_valid_info(info) and self.__is_name_unique(info, self.__project_prefs['info'])

    def __can_add_project_mapping(self, mapping: dict):
        return self.__is_valid_mapping(mapping) and self.__is_mapping_unique(mapping, self.__project_prefs['mapping'])

    def __can_copy_infos_to_defaults(self) -> bool:
        return len(self.__project_infos.selected_items) > 0

    def __can_copy_infos_to_project(self) -> bool:
        return len(self.__default_infos.selected_items) > 0

    def __can_copy_mappings_to_defaults(self) -> bool:
        return len(self.__project_mappings.selected_items) > 0

    def __can_copy_mappings_to_project(self) -> bool:
        return len(self.__default_mappings.selected_items) > 0

    def __copy_infos_to_defaults(self):
        self.__default_infos.items.suspend_events()
        for info_view_model in self.__project_infos.selected_items:
            self.__try_delete_info_from_list(info_view_model, self.__default_infos.items)
            self.__default_infos.items.append(InfoViewModel(copy.deepcopy(info_view_model.model)))
        self.__default_infos.items.resume_events()

    def __copy_infos_to_project(self):
        self.__project_infos.items.suspend_events()
        for info_view_model in self.__default_infos.selected_items:
            self.__try_delete_info_from_list(info_view_model, self.__project_infos.items)
            self.__project_infos.items.append(InfoViewModel(copy.deepcopy(info_view_model.model)))
        self.__project_infos.items.resume_events()

    @staticmethod
    def __try_delete_info_from_list(info_view_model: InfoViewModel, info_view_models: ObservableList):
        # noinspection PyTypeChecker
        info_repr = DProjectInfo.info_repr(info_view_model.model)
        info_reprs = [DProjectInfo.info_repr(info_vm.model) for info_vm in info_view_models]
        if info_repr in info_reprs:
            info_view_models.pop(info_reprs.index(info_repr))

    def __copy_mappings_to_defaults(self):
        self.__default_mappings.items.suspend_events()
        for mapping_view_model in self.__project_mappings.selected_items:
            self.__try_delete_mapping_from_list(mapping_view_model, self.__default_mappings.items)
            self.__default_mappings.items.append(MappingViewModel(copy.deepcopy(mapping_view_model.model)))
        self.__default_mappings.items.resume_events()

    def __copy_mappings_to_project(self):
        self.__project_mappings.items.suspend_events()
        for mapping_view_model in self.__default_mappings.selected_items:
            self.__try_delete_mapping_from_list(mapping_view_model, self.__project_mappings.items)
            self.__project_mappings.items.append(MappingViewModel(copy.deepcopy(mapping_view_model.model)))
        self.__project_mappings.items.resume_events()

    @staticmethod
    def __try_delete_mapping_from_list(mapping_view_model: MappingViewModel, mapping_view_models: ObservableList):
        # noinspection PyTypeChecker
        mapping_repr = DProjectInfo.mapping_repr(mapping_view_model.model)
        mapping_reprs = [DProjectInfo.mapping_repr(mapping_vm.model) for mapping_vm in mapping_view_models]
        if mapping_repr in mapping_reprs:
            mapping_view_models.pop(mapping_reprs.index(mapping_repr))

    @staticmethod
    def __is_valid_info(info: dict):
        return isinstance(info['@name'], str) and info['@name'] != ''

    @staticmethod
    def __is_name_unique(info: dict, infos: list):
        return DProjectInfo.info_repr(info) not in (DProjectInfo.info_repr(info) for info in infos)

    @staticmethod
    def __is_valid_mapping(mapping: dict):
        is_record_valid = isinstance(mapping['@record'], str) and mapping['@record'] != ''
        is_field_valid = isinstance(mapping['@field'], str) and mapping['@field'] != ''
        return is_record_valid and is_field_valid

    @staticmethod
    def __is_mapping_unique(mapping: dict, mappings: list):
        return DProjectInfo.mapping_repr(mapping) not in (DProjectInfo.mapping_repr(mapping) for mapping in mappings)


@ActivePlugInInfo(version=__version__)
@Security(version=__version__.split('.')[0], dongles={'906323', 'G02ZXY', '900901', '900907', '903230', 'AC631C'})
def run():
    if not Document().saved:
        Alert(AlertType.CRITICAL, 'DProjectInfo only works for saved documents', 'Please save first.').show()
    else:
        try:
            DProjectInfo().initialize()
            plugin_prefs, drawing_prefs, dialog = __load_prefs_and_create_dialog()
        except VSException:
            PlugInFileVsExceptionAlert(ActivePlugIn().name).show()
        except FileNotFoundError as e:
            PlugInFileFileNotFoundErrorAlert(e.filename).show()
        except PermissionError as e:
            PlugInFilePermissionErrorAlert(e.filename).show()
        except OSError as e:
            PlugInFileOsErrorAlert(e.filename).show()
        else:
            if dialog.show():
                __save_preferences(plugin_prefs, drawing_prefs)
                __update_project_fields(drawing_prefs['prefs'])
